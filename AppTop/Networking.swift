//
//  Networking.swift
//  AppTop
//
//  Created by SYS_CIBER_ADMIN on 16/04/2018.
//  Copyright © 2018 Katty Quintero. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class Networking {
    func getTopList(completion: @escaping ([AppTopModel]) -> Void) {
       
        Alamofire.request("https://rss.itunes.apple.com/api/v1/ve/ios-apps/top-free/all/100/explicit.json").responseJSON { response in
            
            let json = JSON(response.result.value as Any)
            let dicc = json["feed"].dictionary
            let arrayA = dicc!["results"]?.array
            
            if let arrayModels = arrayA {
                let appsTop = arrayModels.map { AppTopModel(imagen: $0["artworkUrl100"].stringValue,
                                                            name: $0["name"].stringValue,
                                                            company: $0["artistName"].stringValue,
                                                            free: "free") }
                completion(appsTop)
            }
            
//            for app in arrayA!{
//                let model = AppTopModel(imagen: app["artworkUrl100"].stringValue, name: app["name"].stringValue, company: app["artistName"].stringValue, free: "free")
//                self.AppArray.append(model)
//            }
        }
    }
}
