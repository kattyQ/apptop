//
//  AppTopModel.swift
//  AppTop
//
//  Created by johann casique on 14/04/2018.
//  Copyright © 2018 Katty Quintero. All rights reserved.
//

import UIKit

class AppTopModel: NSObject {
    
    var imagen: String
    var name: String
    var company: String
    var free: String

    init(imagen: String, name: String, company: String, free: String) {
        self.imagen = imagen
        self.name = name
        self.company = company
        self.free = free
    }

}
