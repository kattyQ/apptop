//
//  AppTopTableViewCell.swift
//  AppTop
//
//  Created by johann casique on 14/04/2018.
//  Copyright © 2018 Katty Quintero. All rights reserved.
//

import UIKit

class AppTopTableViewCell: UITableViewCell {

    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var companyLabel: UILabel!
    @IBOutlet weak var freeLabel: UILabel!
    
   public func configCell(model: AppTopModel){
        let url = URL(string: model.imagen)
        imgView.kf.setImage(with: url)
        nameLabel.text = model.name
        companyLabel.text = model.company
        freeLabel.text = model.free
    }
    
}
