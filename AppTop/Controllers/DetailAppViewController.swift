//
//  DetailAppViewController.swift
//  AppTop
//
//  Created by johann casique on 14/04/2018.
//  Copyright © 2018 Katty Quintero. All rights reserved.
//

import UIKit

class DetailAppViewController: UIViewController {

    @IBOutlet weak var imgDetail: UIImageView!
    @IBOutlet weak var nameDetail: UILabel!
    @IBOutlet weak var companyDetail: UILabel!
    @IBOutlet weak var freeDetail: UILabel!
    
    public var model: AppTopModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let modeApp = model {
            let url = URL(string: modeApp.imagen)
            self.imgDetail.kf.setImage(with: url)
            self.nameDetail.text = modeApp.name
            self.companyDetail.text = modeApp.company
            self.freeDetail.text = modeApp.free
        }
        
        
        
    }

}
