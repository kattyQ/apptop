//
//  ViewController.swift
//  AppTop
//
//  Created by johann casique on 14/04/2018.
//  Copyright © 2018 Katty Quintero. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Kingfisher

class ViewController: UIViewController,UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    var AppArray = [AppTopModel]()
    override func viewDidLoad() {
        super.viewDidLoad()
       getJson()
    }
    //=================================
    // MARK: - FUNCTIONS
    //=================================
    func getJson(){
        
        Networking().getTopList { (arrayApps) in
            self.AppArray = arrayApps
            self.tableView.reloadData()
        }
        
    }
    //=================================
    // MARK: - DATASOURCE
    //=================================
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return AppArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! AppTopTableViewCell
        cell.configCell(model: AppArray[indexPath.row])
        return cell
    }
    //=================================
    // MARK: DELEGATE
    //=================================
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let detailVC = storyboard?.instantiateViewController(withIdentifier: "detailVC") as! DetailAppViewController
        detailVC.model = AppArray[indexPath.row]
        navigationController?.pushViewController(detailVC, animated: true)
    }
}

